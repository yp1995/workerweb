<?php
use Workerman\Worker;
use Workerman\Connection\TcpConnection;
// use Workerman\Protocols\Http\Request;
use Misyuan\Request;
use Workerman\Protocols\Http;

require_once __DIR__ . '/vendor/autoload.php';

define('APP_ROOT', 'app');
define('DIR', "\\");

$worker = new Worker('http://0.0.0.0:8080');
$worker->count = 4;
$worker->onWorkerStart = function ($worker) {
     Http::requestClass(Request::class);
};

$worker->onMessage = function(TcpConnection $connection, Request $request)
{
 
    $Test = new Misyuan\Route(['r'=>$request->path()],$request);
    // var_dump();
    // $request为请求对象，这里没有对请求对象执行任何操作直接返回hello给浏览器
    $connection->send($Test->run());
};

// 运行worker
Worker::runAll();