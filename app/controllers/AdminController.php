<?php
namespace app\controllers;


class AdminController
{
    public function index($request)
    {
        $name = $request->get("name");
       return json_encode(["code"=>0,'msg'=>'ok','data'=>$name]);
    }

   
    public function view($request)
    {
        return json_encode( ['name' => 'webman']);
    }

    public function json(Request $request)
    {
        return json(['code' => 0, 'msg' => 'ok']);
    }

}
