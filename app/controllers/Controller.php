<?php
namespace app\controllers;


class Controller
{
    public function index($request)
    {
        $name = $request->get("name");
       return json_encode(["code"=>0,'msg'=>'ok','data'=>$name]);
    }

    public function view(Request $request)
    {
        return view('index/view', ['name' => 'webman']);
    }

    public function json(Request $request)
    {
        return json(['code' => 0, 'msg' => 'ok']);
    }

}
