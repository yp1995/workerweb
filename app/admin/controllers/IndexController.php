<?php
namespace app\admin\controllers;
use Misyuan\Request;

class IndexController
{
    public function index(Request $request)
    {
        $name = $request->get("name");
       return json_encode(["code"=>0,'msg'=>'ok','data'=>$name]);
    }

    public function view(Request $request)
    {
        return view('index/view', ['name' => 'workerweb']);
    }

    public function json(Request $request)
    {
        return json(['code' => 0, 'msg' => 'ok']);
    }

}
