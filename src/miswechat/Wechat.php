<?php
namespace MisWechat;
 
 
/**
 * Created by PhpStorm.
 * User: dell1
 * Date: 2019/7/30
 * Time: 22:23
 */
class Wechat
{
    private $appid = "";
    private $secret = "";
    private $code ="";
    function __construct($appid,$secret) {
       $this->appid =$appid;
       $this->secret =$secret;
       
   }
   function Getauthorize($redirect_uri)
   {
       $appid = $this->appid;
       $redirect_uri = urlencode($redirect_uri);//重定向地址
       $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=$appid&redirect_uri=$redirect_uri&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
       header("Location:" . $url);
   }
   public function Setcode($val){
       $this->code = $val;
   }
   public function Getopenid()
   {
      $appid = $this->appid;;  //填写你公众号的appid
      $secret = $this->secret;  //填写你公众号的secret
      $code = $this->code;
      $oauth2Url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=$appid&secret=$secret&code=$code&grant_type=authorization_code";
      $oauth2 =  $this->getJson($oauth2Url);
      return $oauth2;
   }
   public function Getuserinfo($oauth2)
   {
       $access_token = $oauth2["access_token"]; 
       $openid = $oauth2['openid']; 
       $get_user_info_url = "https://api.weixin.qq.com/sns/userinfo?access_token=$access_token&openid=$openid&lang=zh_CN";
       $userinfo = $this->getJson($get_user_info_url);
       return $userinfo;
   }
   //缓存存的地方
   public function Gettoken($Gettoken="")
   {
       $appid = $this->appid;;  //填写你公众号的appid
       $secret = $this->secret;  //填写你公众号的secret
       $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$appid."&secret=".$secret;
       //存redis 缓存 1小时
    
           $Gettoken = $this->getJson($url);
       if(empty($Gettoken["access_token"])){
           return $Gettoken;
       }
       return $Gettoken["access_token"];
   }
    private function get_php_file($filename) {
      return trim(file_get_contents($filename));
   }
   // 把token 存储到文件中
   private function set_php_file($filename, $content) {
       $fp = fopen($filename, "w");
        fwrite($fp,  $content);
        fclose($fp);
   }
   
   public function Sendtemplate($openid,$template_id,$info)
   {
       $access_token = $this->Gettoken();
       $url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=".$access_token;
       $arr['touser'] = $openid;
       $arr['template_id'] = $template_id;
       $arr['topcolor'] = "#FF0000";
       $arr['data'] = $info;
       $data = json_encode($arr); 
       $res_info = $this->curl_post($url,$data);
       return  $res_info;
   }
   public function getJson($url){
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
       curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       $output = curl_exec($ch);
       curl_close($ch);
       return json_decode($output, true);
    }
     public function curl_post($url , $data){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        // POST数据

        curl_setopt($ch, CURLOPT_POST, 1);

        // 把post的变量加上

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $output = curl_exec($ch);

        curl_close($ch);
        return json_decode($output, true);
        return $output;

    }
//   public

   function __destruct() {
       
   }
    
}