<?php

use Misyuan\ThinkPHP;
use Misyuan\App;
use Misyuan\Config;
use \Workerman\Protocols\Http\Response;
define('BASE_PATH', realpath(__DIR__ . '/../'));

/**
 * @return string
 */
function base_path()
{
    return BASE_PATH;
}
function config($key = null, $default = null)
{
    return Config::get($key, $default);
}
/**
 * @return string
 */
function app_path()
{
    return BASE_PATH . DIRECTORY_SEPARATOR . 'app';
}

/**
 * @return string
 */
function public_path()
{
    return BASE_PATH . DIRECTORY_SEPARATOR . 'public';
}

/**
 * @return string
 */
function config_path()
{
    return BASE_PATH . DIRECTORY_SEPARATOR . 'config';
}

/**
 * @return string
 */
function runtime_path()
{
    return BASE_PATH . DIRECTORY_SEPARATOR . 'runtime';
}

/**
 * @param $template
 * @param array $vars
 * @param null $app
 * @return Response
 */
function view($template, $vars = [], $app = null)
{
    static $handler;
    if (null === $handler) {
        $handler =  ThinkPHP::class;
    }
    return new Response(200, [], $handler::render($template, $vars, $app));
}
function request()
{
    return App::request();
}
function app(){
    return App::app();
}
?>